/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import Modelo.Address;
import Modelo.Customer;
import Modelo.CustomerDTO;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Harold
 */
public class CustomerDAO {

    AddressJpaController ad = new AddressJpaController();
    CustomerJpaController cu = new CustomerJpaController();

    public List<CustomerDTO> readCustomer() {
        List<Address> a = ad.findAddressEntities();
        List<Customer> c = cu.findCustomerEntities();
        List<CustomerDTO> customers = new ArrayList<CustomerDTO>();
        
        for (byte i = 0; i < a.size(); i++) {
            String full = c.get(i).getFirstname() + " " + c.get(i).getLastname();
            CustomerDTO dto = new CustomerDTO(a.get(i).getId(), full, 
                    a.get(i).getCountry(), a.get(i).getAddress(), a.get(i).getZipCode());
            customers.add(dto);
        }
        
        return customers;
    }
}
