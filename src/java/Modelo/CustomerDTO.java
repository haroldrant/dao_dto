/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.io.Serializable;

/**
 *
 * @author JeAnA
 */
public class CustomerDTO implements Serializable{
	
    private Long id;
    private String FullName;
    private String country;
    private String Address;
    private String zipCode;

    public CustomerDTO() {
    }

    public CustomerDTO(Long id, String FullName, String country, String Address, String zipCode) {
        this.id = id;
        this.FullName = FullName;
        this.country = country;
        this.Address = Address;
        this.zipCode = zipCode;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String FullName) {
        this.FullName = FullName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    
    
}
